import { withWrapper } from "create-react-server/wrapper";
import { connect } from "react-redux";
import DashboardComponent from "./Dashboard";

const mapStateToProps = state => {
  return {};
};

const Dashboard = withWrapper(connect(mapStateToProps)(DashboardComponent));

export default Dashboard;
