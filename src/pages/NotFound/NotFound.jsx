import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Helmet from "../../components/common/Helmet";

const Wrapper = styled.div`
  background-color: blue;
`;

export default class NotFound extends React.Component {
  static notFound = true;

  render() {
    if (this.props.staticContext) {
      this.props.staticContext.status = 404;
    }

    return (
      <Wrapper>
        <Helmet title="Page Not Found" />
        <h1>Page Not Found</h1>
        <p>
          Go to <Link to="/">index page</Link>.
        </p>
      </Wrapper>
    );
  }
}
