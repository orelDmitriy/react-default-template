import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

import Helmet from "../../components/common/Helmet";

const Wrapper = styled.div`
  background-color: green;
`;
const Block = styled.div`
  width: 100%;
  height: 200px;
  background-color: #00ffff;
`;

export default class Dashboard extends Component {
  static async getInitialProps() {
    return { custom: "custom" + Date.now() };
  }

  state = {
    count: 0
  };

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.count !== this.state.count;
  }

  addBlock = () => {
    this.setState({
      count: this.state.count + 1
    });
  };

  getBlocks = () => {
    const result = [];
    for (let i = 0; i < this.state.count; i++) {
      result.push(<Block key={i} onClick={this.addBlock} />);
    }
    return result;
  };

  render() {
    return (
      <Wrapper onClick={this.addBlock}>
        <Helmet title="Index" />
        <h1>Hello world!</h1>
        <p>
          Go to <Link to="/test">test page</Link>.
        </p>
        {this.getBlocks()}
      </Wrapper>
    );
  }
}
