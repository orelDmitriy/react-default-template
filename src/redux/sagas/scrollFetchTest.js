import {takeEvery, put, call} from 'redux-saga/effects';

import {SCROLL_FETCH, scrollLoadedDataSet} from '../actions/scroll';
import {delay} from './utils';

export function* scrollFetchWatches() {
		yield takeEvery(SCROLL_FETCH, scrollFetchWorker);
}

function* scrollFetchWorker(action) {
		yield call(delay(2000));
		yield put(scrollLoadedDataSet(action.name, false));
}