import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { App } from "./components/app/App";
import NotFound from "./pages/NotFound/NotFound";
import Dashboard from "./pages/Dashboard/Dashboard.connect";

export default (
  <App>
    <Switch>
      <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
      <Route exact path="/dashboard" component={Dashboard} />
      <Route path="*" component={NotFound} />
    </Switch>
  </App>
);
