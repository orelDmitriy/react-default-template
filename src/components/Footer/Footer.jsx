import React, { PureComponent } from "react";
import styled from "styled-components";

export const FOOTER_HEIGHT = 60;

const Wrapper = styled.header`
  width: 100%;
  height: ${FOOTER_HEIGHT}px;
`;

export class Footer extends PureComponent {
  render() {
    return <Wrapper>Footer</Wrapper>;
  }
}
