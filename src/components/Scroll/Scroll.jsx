import React, { Component } from "react";
import ReactResizeDetector from "react-resize-detector";
import { Scrollbars } from "react-custom-scrollbars";
import { throttle } from "lodash/function";
import PropTypes from "prop-types";

export class Scroll extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired
  };

  state = {
    resize: 0
  };

  componentDidMount() {
    this.props.initScroll(this.props.name);
  }

  componentWillUnmount() {
    this.props.removeScroll(this.props.name);
    this.onScrollFrame.cancel();
    this.onBodyResize.cancel();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.children !== this.props.children) {
      return true;
    }
    return nextState.resize !== this.state.resize;
  }

  onScrollFrame = throttle(values => {
    const {
      loadingData,
      hasMore,
      pixelsForFetch,
      scrollFetch,
      name
    } = this.props;
    const delta = values.scrollHeight - values.scrollTop;

    if (hasMore && !loadingData && delta < pixelsForFetch) {
      scrollFetch(name);
    }
  }, 500);

  onBodyResize = throttle(() => {
    this.setState({
      resize: this.state.resize + 1
    });
  });

  render() {
    const { children } = this.props;
    return (
      <Scrollbars onScrollFrame={this.onScrollFrame}>
        <div className="scrollBody">
          {children}
          <ReactResizeDetector handleHeight onResize={this.onBodyResize} />
        </div>
      </Scrollbars>
    );
  }
}
