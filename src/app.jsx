import React from "react";
import { Provider } from "react-redux";
import { WrapperProvider } from "create-react-server/wrapper";
import createStore from "./redux/configStore";
import createMemoryHistory from "history/createMemoryHistory";
import createBrowserHistory from "history/createBrowserHistory";
import { ConnectedRouter } from "react-router-redux";
import routes from "./routes";

export const isBrowser = typeof window !== "undefined";

export default ({ state, props, req }) => {
  let history = isBrowser ? createBrowserHistory() : createMemoryHistory();

  if (isBrowser) {
    return (
      <Provider store={createStore(state, history)}>
        <ConnectedRouter history={history}>
          <WrapperProvider initialProps={props}>{routes}</WrapperProvider>
        </ConnectedRouter>
      </Provider>
    );
  }

  return (
    <Provider store={createStore(state, history)}>
      <WrapperProvider initialProps={props}>{routes}</WrapperProvider>
    </Provider>
  );
};
