import { applyMiddleware, compose, createStore } from "redux";
import promiseMiddleware from "redux-promise-middleware";
import { routerMiddleware } from "react-router-redux";
import createSagaMiddleware from "redux-saga";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";

import mainReduser from "./redusers/index";
import { isBrowser } from "../app";
import rootSaga from "./sagas/root";

export default function configureStore(initialState = {}, history) {
  const router = routerMiddleware(history);
  const sagaMiddleware = createSagaMiddleware();
  let middlewares = [
    promiseMiddleware({
      promiseTypeSuffixes: ["PENDING", "SUCCESS", "ERROR"]
    }),
    thunk,
    router,
    sagaMiddleware
  ];

  if (isBrowser) {
    middlewares.push(
      createLogger({
        collapsed: true
      })
    );
  }

  let store = createStore(
    mainReduser,
    initialState,
    compose(
      applyMiddleware(...middlewares),
      isBrowser && window["devToolsExtension"]
        ? window["devToolsExtension"]()
        : f => f
    )
  );

  sagaMiddleware.run(rootSaga);
  return store;
}
