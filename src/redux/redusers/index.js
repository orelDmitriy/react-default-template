import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import scrolls from "./scrolls";

export default combineReducers({
  router: routerReducer,
  scrolls
});
