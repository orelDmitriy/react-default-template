import React, { PureComponent } from "react";
import styled from "styled-components";

export const HEADER_HEIGHT = 60;

const Wrapper = styled.header`
  width: 100%;
  height: ${HEADER_HEIGHT}px;
`;

export class Header extends PureComponent {
  render() {
    return <Wrapper>Header</Wrapper>;
  }
}
