import { fork } from "redux-saga/effects";
import { scrollFetchWatches } from "./scrollFetchTest";

export default function* rootSaga() {
  yield fork(scrollFetchWatches);
}
