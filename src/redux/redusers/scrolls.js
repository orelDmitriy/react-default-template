import {SCROLL_INIT, SCROLL_REMOVE, SCROLL_FETCH, SCROLL_LOADING_DATA_SET} from '../actions/scroll';

export const defaultScrollState = {
	  hasMore: true,
		loadingData: false,
		pixelsForFetch: 600
};

export default function scrolls(state = {}, action) {
		let newState;
		let scroll;
		switch (action.type) {
				case SCROLL_INIT:
						return {...state, [action.name]: defaultScrollState};
				case SCROLL_REMOVE:
						newState = {...state};
						delete newState[action.name];
						return newState;
				case SCROLL_FETCH:
						newState = {...state};
						scroll = newState[action.name];
						if (scroll) {
								scroll.loadingData = true;
						}
						return newState;
				case SCROLL_LOADING_DATA_SET:
						newState = {...state};
						scroll = newState[action.name];
						if (scroll) {
								scroll.loadingData = action.loadingData;
						}
						return newState;
				default:
						return state;
		}
}