# Development

Runs Webpack Dev Server, **no Server Side Rendering**.

```bash
$ npm start
```

# Production + Server Side Rendering

The redeploy sequence is as follows:

```bash
$ npm run build
$ npm run server
```

# Redux logger

Redux DevTools for browsers

chrome: https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd
