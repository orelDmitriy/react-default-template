import connect from "react-redux/es/connect/connect";
import { createSelector } from "reselect";

import {
  initScroll,
  removeScroll,
  scrollFetch
} from "../../redux/actions/scroll";
import { scrollsSelector } from "../../selectors/redusersSelectors";
import { defaultScrollState } from "../../redux/redusers/scrolls";
import { Scroll } from "./Scroll";

const InfinityScroll = connect(
  createSelector(
    [scrollsSelector, (state, props) => props.name],
    (scrolls, scrollName) => ({
      ...(scrolls[scrollName] || defaultScrollState)
    })
  ),
  {
    initScroll,
    removeScroll,
    scrollFetch
  }
)(Scroll);

export default InfinityScroll;
