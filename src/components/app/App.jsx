import React from "react";
import styled from "styled-components";

import { Header, HEADER_HEIGHT } from "../Header/Header";
import "./global.css";
import { Footer, FOOTER_HEIGHT } from "../Footer/Footer";
import InfinityScroll from "../Scroll/Scroll.connect";

const Wrapper = styled.div`
  position: relative;
  width: 100vw;
  height: 100vh;
  display: grid;
  grid-template-rows: ${HEADER_HEIGHT}px 1fr;
  overflow: hidden;
`;

const Body = styled.main`
  position: relative;
  width: 100%;
  min-height: calc(100vh - ${HEADER_HEIGHT}px - ${FOOTER_HEIGHT}px);
  overflow-x: hidden;
`;

export const App = ({ children }) => {
  return (
    <Wrapper>
      <Header />
      <InfinityScroll name="body_scroll">
        <Body>{children}</Body>
        <Footer />
      </InfinityScroll>
    </Wrapper>
  );
};
