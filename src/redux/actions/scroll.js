export const SCROLL_INIT = 'SCROLL_INIT';
export function initScroll(name) {
		return {
				type: SCROLL_INIT,
				name
		}
}

export const SCROLL_REMOVE = 'SCROLL_REMOVE';
export function removeScroll(name) {
		return {
				type: SCROLL_REMOVE,
				name
		}
}

export const SCROLL_FETCH = 'SCROLL_FETCH';
export function scrollFetch(name) {
		return {
				type: SCROLL_FETCH,
				name
		}
}

export const SCROLL_LOADING_DATA_SET = 'SCROLL_LOADING_DATA_SET';
export function scrollLoadedDataSet(name, loadingData) {
		return {
				type: SCROLL_LOADING_DATA_SET,
				name,
				loadingData
		}
}